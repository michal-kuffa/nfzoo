======================
Netflow message fields
======================

.. list-table:: Netflow message fields
   :header-rows: 1
   :widths: 15 50

   * - Field name
     - Description

   * - tfs
     - Date first seen, Start Time - first seen
   
   * - ts
     - Date first seen, Start Time - first seen
   
   * - te
     - Date last seen, End Time    - last seen
   
   * - tr
     - Date flow received, Received Time
   
   * - td
     - Duration, Duration
   
   * - exp
     - Exp ID, Exporter SysID
   
   * - pr
     - Proto, Protocol
   
   * - sa
     - Src IP Addr, Source Address
   
   * - da
     - Dst IP Addr, Destination Address
   
   * - sn
     - Src Network, Source Address applied source netmask
   
   * - dn
     - Dst Network, Destination Address applied source netmask
   
   * - nh
     - Next-hop IP, Next-hop IP Address
   
   * - nhb
     - BGP next-hop IP, BGP Next-hop IP Address
   
   * - ra
     - Router IP, Router IP Address
   
   * - sap
     - Src IP Addr:Port, Source Address:Port
   
   * - dap
     - Dst IP Addr:Port, Destination Address:Port
   
   * - sp
     - Src Pt, Source Port
   
   * - dp
     - Dst Pt, Destination Port
   
   * - it
     - ICMP-T, ICMP type
   
   * - ic
     - ICMP-C, ICMP code
   
   * - sas
     - Src AS, Source AS
   
   * - das
     - Dst AS, Destination AS
   
   * - nas
     - Next AS, Next AS
   
   * - pas
     - Prev AS, Previous AS
   
   * - in
     - Input, Input Interface num
   
   * - out
     - Output, Output Interface num
   
   * - pkt
     - Packets, Packets - default input - compat
   
   * - ipkt
     - In Pkt, In Packets
   
   * - opkt
     - Out Pkt, Out Packets
   
   * - byt
     - Bytes, Bytes - default input - compat
   
   * - ibyt
     - In Byte, In Bytes
   
   * - obyt
     - Out Byte, In Bytes
   
   * - fl
     - Flows, Flows
   
   * - flg
     - Flags, TCP Flags
   
   * - tos
     - Tos, Tos - compat
   
   * - stos
     - STos, Tos - Src tos
   
   * - dtos
     - DTos, Tos - Dst tos
   
   * - dir
     - Dir, Direction: ingress
   
   * - smk
     - SMask, Src mask
   
   * - dmk
     - DMask, Dst mask
   
   * - fwd
     - Fwd, Forwarding Status
   
   * - svln
     - SVlan, Src Vlan
   
   * - dvln
     - DVlan, Dst Vlan
   
   * - ismc
     - In src MAC Addr, Input Src Mac Addr
   
   * - odmc
     - Out dst MAC Addr, Output Dst Mac Addr
   
   * - idmc
     - In dst MAC Addr, Input Dst Mac Addr
   
   * - osmc
     - Out src MAC Addr, Output Src Mac Addr
   
   * - mpls1
     - MPLS lbl 1, MPLS Label 1
   
   * - mpls2
     - MPLS lbl 2, MPLS Label 2
   
   * - mpls3
     - MPLS lbl 3, MPLS Label 3
   
   * - mpls4
     - MPLS lbl 4, MPLS Label 4
   
   * - mpls5
     - MPLS lbl 5, MPLS Label 5
   
   * - mpls6
     - MPLS lbl 6, MPLS Label 6
   
   * - mpls7
     - MPLS lbl 7, MPLS Label 7
   
   * - mpls8
     - MPLS lbl 8, MPLS Label 8
   
   * - mpls9
     - MPLS lbl 9, MPLS Label 9
   
   * - mpls10
     - MPLS lbl 10, MPLS Label 10
   
   * - mpls
     - MPLS labels 1-10, All MPLS labels
   
   * - bps
     - bps, bps - bits per second
   
   * - pps
     - pps, pps - packets per second
   
   * - bpp
     - Bpp, bpp - Bytes per package
   
   * - eng
     - engine, Engine TypeID
   
   * - nfc
     - Conn-ID, NSEL connection ID
   
   * - tevt
     - Event time, NSEL Flow start time
   
   * - evt
     - Event, NSEL event
   
   * - xevt
     - XEvent, NSEL xevent
   
   * - msec
     - Event Time, NSEL event time in msec
   
   * - iacl
     - Ingress ACL, NSEL ingress ACL
   
   * - eacl
     - Egress ACL, NSEL egress ACL
   
   * - xsa
     - X-late Src IP, NSEL XLATE src IP
   
   * - xda
     - X-late Dst IP, NSEL XLATE dst IP
   
   * - xsp
     - XsPort, NSEL XLATE src port
   
   * - xdp
     - XdPort, NSEL SLATE dst port
   
   * - xsap
     - X-Src IP Addr:Port, Xlate Source Address:Port
   
   * - xdap
     - X-Dst IP Addr:Port, Xlate Destination Address:Port
   
   * - uname
     - UserName, NSEL user name
   
   * - nevt
     - Event, NAT event
   
   * - vrf
     - I-VRF-ID, NAT ivrf ID - compatible
   
   * - ivrf
     - I-VRF-ID, NAT ivrf ID
   
   * - evrf
     - E-VRF-ID, NAT ivrf ID
   
   * - nsa
     - X-late Src IP, NAT XLATE src IP
   
   * - nda
     - X-late Dst IP, NAT XLATE dst IP
   
   * - nsp
     - XsPort, NAT XLATE src port
   
   * - ndp
     - XdPort, NAT SLATE dst port
   
   * - nsap
     - X-Src IP Addr:Port, NAT Xlate Source Address:Port
   
   * - ndap
     - X-Dst IP Addr:Port, NAT Xlate Destination Address:Port
   
   * - pbstart
     - Pb-Start, Port block start
   
   * - pbend
     - Pb-End, Port block end
   
   * - pbstep
     - Pb-Step, Port block step
   
   * - pbsize
     - Pb-Size, Port block size
   
   * - cl
     - C Latency, client latency
   
   * - sl
     - S latency, server latency
   
   * - al
     - A latency, app latency
