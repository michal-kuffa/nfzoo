============
Installation
============


Dependencies
============

For nfcapd compilation you'll need:

::

  apt-get install build-essential flex libtool \
                  autoconf libtool pkg-config


For elasticsearch you'll need java runtime:

::

  apt-get install openjdk-7-jre


To create separate enviroment for python zmq subscriber
you'll need:

::

  apt-get install python-dev python-virtualenv


Prepare directories
===================

::

  mkdir /data/{es,nfdump}


* /data/es

  * Elasticsearch data directory
  * Permissions: ``chown -R elasticsearch:elasticsearch /data/es``

 
* /data/nfdump

  * nfcapd data directory
  * write permissions for user taht runs nfcapd



Running nfcapd (with zmq)
=========================

::

  ./bin/nfcapd -l /data/nfdump -E -q 'tcp://127.0.0.1:5556'
