========
Overview
========

.. image:: _static/netflow_routers.png


Components
==========

**nfcapd**
        Customized nfcapd which bind publisher side of zmq socket and all received
        netflow messages after serializing them to json format, publishes there for
        all connected subscribers to process them

**pipe_to_es**
        Subscribes to nfcapd and *pipes* all messages to Elasticsearch. It works in 
        bulk mode thus collects number of messages and using Elasticsearch's bulk API
        save them into time-based index *nf-dump-%Y.%m.%d-%H*


All netflow capable routers send messages to our customized nfcapd which then


