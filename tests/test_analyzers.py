#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

import time
import pytest
import logging

from nfzoo.analyzers.base import AnalyzerBase
from nfzoo.analyzers import mixins


class TestAnalyzerBase:

    def test_init(self):
        analyzer = AnalyzerBase()
        assert analyzer._id is None
        assert not analyzer.args
        assert not analyzer.kwargs

    def test_get_level(self):
        a = AnalyzerBase()
        assert a.get_level() == logging.ERROR
        a = AnalyzerBase(level=logging.WARNING)
        assert a.get_level() == logging.WARNING

    def test_get_msg(self):
        a = AnalyzerBase()
        with pytest.raises(AttributeError):
            a.get_msg()

        a = AnalyzerBase(msg="msg")
        assert a.get_msg() == "msg"

        class TA(AnalyzerBase):
            default_msg = "msg"
        a = TA()
        assert a.get_msg() == "msg"

    def test_flow_record(self):
        a = AnalyzerBase()
        a.emit(None, None)
        a.add_flow_record(None, None)


class TestPassThresholdMixin:

    def test_setting_value_getter(self):
        class TA(mixins.PassThresholdMixin, AnalyzerBase):
            pass

        a = TA(
            time_counter_interval={"seconds": 1},
            threshold=5,
            record_value_getter=lambda x, y,z: 5,
        )
        a.add_flow_record("ip", "flow_record")
        a.add_flow_record("ip", "flow_record")
        assert a.counter["ip"] == 10


    def test_record_getter_from_string(self):
        class TA(mixins.PassThresholdMixin, AnalyzerBase):
            pass

        a = TA(
            time_counter_interval={"seconds": 1},
            threshold=5,
            record_value_getter='nfzoo.analyzers.mixins.ValueGetter',
        )
        a.add_flow_record("ip", "flow_record")
        a.add_flow_record("ip", "flow_record")
        a.add_flow_record("ip", "flow_record")
        a.add_flow_record("ip", "flow_record")
        assert a.counter["ip"] == 4

    def test_add_flow_record(self, monkeypatch):

        class TE(Exception):
            pass

        def emit(*args, **kwargs):
            raise TE("Emit called.")

        class TA(mixins.PassThresholdMixin, AnalyzerBase):
            pass

        a = TA()

        # XXX (beezz): Provide way to validate init args, kwargs.
        with pytest.raises(TypeError):
            a.add_flow_record("ip", "flow_record")

        a = TA(
            time_counter_interval={"seconds": 1},
            threshold=5,
        )

        # simulate time passing
        a.add_flow_record("ip", "flow_record")
        time.sleep(1.5)
        a.add_flow_record("ip", "flow_record")
        assert not a.counter

        # clear counter after check
        for _ in range(10):
            a.add_flow_record("ip", "flow_record")
        assert a.counter
        monkeypatch.setattr(a, 'emit', emit)
        with pytest.raises(TE):
            a.counter_check()


class TestMemoryMixin:

    def test_counter_memory(self):
        class TA(mixins.ThresholdMemoryMixin, AnalyzerBase):
            pass
        a = TA(
            time_counter_interval={"seconds": 0},
            threshold=5,
        )
        # test dummy sampling
        assert len(a.counter_memory.data) == 0
        for _ in range(48):
            a.add_flow_record("ip", "flow_record")
        assert len(a.counter_memory.data) == 4
        assert a.counter_memory.get_for_ip("ip") == 1

    def test_percentile_calculation(self):
        class TA(mixins.ThresholdMemoryMixin, AnalyzerBase):
            pass
        a = TA(
            time_counter_interval={"days": 5},
            threshold=5,
        )
        assert len(a.counter_memory.data) == 0
        assert a.counter_memory.get_for_ip("ip", percentile=0.5) is None
        ranges = [1] * 95 + [10] * 5
        for r in ranges:
            for _ in range(12):
                for _ in range(r):
                    a.add_flow_record("ip", "flow_record")
                a.counter_reset()
        assert a.counter_memory.get_for_ip("ip", percentile=0.94) == 1
        assert a.counter_memory.get_for_ip("ip", percentile=0.96) == 10

    def test_counter_check_ip(self):
        class TA(mixins.ThresholdMemoryMixin, AnalyzerBase):
            pass
        a = TA(time_counter_interval={"days": 5})
        assert len(a.counter_memory.data) == 0
        assert a.counter_memory.get_for_ip("ip", percentile=0.5) is None
        ranges = [100] * 95 + [200] * 5
        for r in ranges:
            for _ in range(12):
                for _ in range(r):
                    a.add_flow_record("ip", "flow_record")
                a.counter_reset()
        assert a.counter_memory.get_for_ip("ip", percentile=0.94) == 100
        assert a.counter_memory.get_for_ip("ip", percentile=0.96) == 200
        for _ in range(160):
            a.add_flow_record("ip", "flow_record")
        assert a.get_threshold(ip="ip") == 150
        assert a.get_ip_counter(ip="ip") == 160
        assert a.counter_check_ip(ip="ip") is True

    def test_percentile_caching(self):
        class TA(mixins.ThresholdMemoryMixin, AnalyzerBase):
            pass
        a = TA(
            time_counter_interval={"days": 5},
            threshold=5,
        )
        assert len(a.counter_memory.data) == 0

        # just initialized nothing in cache
        assert ("ip", 0.5) not in a.counter_memory._cache
        # empty list result should be None
        assert a.counter_memory.get_for_ip("ip", percentile=0.5) is None
        ranges = [1]
        for r in ranges:
            for _ in range(12):
                for _ in range(r):
                    a.add_flow_record("ip", "flow_record")
                a.counter_reset()
        # adding new counters to memry should
        # invalidate cache
        assert ("ip", 0.5) not in a.counter_memory._cache
        assert a.counter_memory.get_for_ip("ip", percentile=0.5) == 1
        assert ("ip", 0.5) in a.counter_memory._cache
        # this call should take value from cache
        assert a.counter_memory.get_for_ip("ip", percentile=0.5) == 1
        assert ("ip", 0.9) not in a.counter_memory._cache
        assert a.counter_memory.get_for_ip("ip", percentile=0.9) == 1
        assert ("ip", 0.9) in a.counter_memory._cache
