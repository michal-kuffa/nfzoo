#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

import pytest


class TestConfig:

    def test_simple(self):
        from nfzoo.config import settings, ConfigException
        with pytest.raises(ConfigException):
            settings.lol

    def test_attrs_bag(self):
        from nfzoo.config import AttrsBag, ConfigException
        ab = AttrsBag(lol="olo")
        ab.__str__()
        ab.__repr__()
        with pytest.raises(ConfigException):
            ab.wtf
