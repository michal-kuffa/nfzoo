'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:SidenavleftCtrl
 * @description
 * # SidenavleftCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('NavCtrl', function ($scope, $location) {
    var self = this;

    self.navLinksTop = [
      {href: '/', name: "Home"},
      {href: '/about', name: "About"}
    ];


    self.changePath = function(path) {
      return $location.path(path);
    }

    self.isActivePath = function(path) {
      console.log($location.path() === path)
      return $location.path() === path;
    }

  });
