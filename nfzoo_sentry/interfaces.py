#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

from sentry.interfaces.base import Interface
from sentry.web.helpers import render_to_string


class NfZooEvent(Interface):

    display_score = 1100
    score = 900

    def get_path(self):
        return 'nfzoo_sentry.interfaces.NfZooEvent'

    def get_hash(self):
        return []

    def to_html(self, event, is_public=False, **kwargs):
        return render_to_string(
            'nfzoo_sentry/nfzoo_event.html',
            {
                'event': event,
                'interface': event.data.get(self.get_path()),
            })
