#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@T500>

"""

nfzoo.keeper
~~~~~~~~~~~~

Holds stats from previous interval and receives new stats.

Combined data results into anomality index and trend statistics.
"""

import IPy
import zmq
import json
import logging
import datetime
import collections

import elasticsearch
from elasticsearch import helpers as es_helpers

from .config import settings


LOGGER = logging.getLogger(name=__file__)


class MessagesHandler(object):

    def __init__(self, zmq_pull, zmq_pub, es=None):
        self.zmq_pull = zmq_pull
        self.zmq_pub = zmq_pub
        self.data = {"data": {}}
        self.es = es

    def _sum_data(self, data):
        sum_data = collections.defaultdict(int)
        for ip, ip_data in data:
            for k, v in ip_data.items():
                sum_data[k] += v
        return sum_data

    def handle_request_msg(self, msg):
        _ip = msg.get("ip")
        msg_id = msg.get("msg_id")
        resp_msg = {
            "msg_id": msg_id,
            "created": self.data.get("created"),
            "rotated": self.data.get("rotated"),
        }
        try:
            req_ip = IPy.IP(_ip)
        except ValueError as exc:
            resp_msg["error"] = exc.message
        else:
            res_data = collections.defaultdict(list)
            for ra, router_data in self.data["data"].items():
                for ip, ip_data in router_data.items():
                    if IPy.IP(ip) in req_ip:
                        res_data[ra].append((ip, ip_data))

            if msg.get("sum"):
                for ra, router_data in res_data.items():
                    res_data[ra] = [[_ip, self._sum_data(router_data)]]

            if msg.get("total"):
                total_data = sum(res_data.values(), [])
                for ra in res_data.keys():
                    del res_data[ra]
                res_data["TOTAL"] = [[_ip, self._sum_data(total_data)]]

            resp_msg["data"] = res_data
        self.zmq_pub.send_json(resp_msg)

    def _union_ips_set(self, od, nd, ra):
        return set(od.get(ra, {}).keys()) | set(nd.get(ra, {}).keys())

    def handle_stats_msg(self, msg):
        _new_data = {
            "created": msg["created"],
            "rotated": msg["rotated"],
            "data": {},
        }
        time_diff = msg["rotated"] - msg["created"]
        tmpd = _new_data["data"]
        field_names = (
            "conn_count",
            "out_conn_count",
            "in_conn_count",
            "in_byt",
            "out_byt",
            "pkt",
        )
        old_data = self.data["data"]
        new_data = msg["data"]
        routers = set(new_data.keys()) | set(old_data.keys())
        for ra in routers:
            tmpd[ra] = {}
            ips = self._union_ips_set(old_data, new_data, ra)
            for ip in ips:
                ip_data = {}
                new_ip_data = new_data.get(ra, {}).get(ip, {"_missing": True})
                old_ip_data = old_data.get(ra, {}).get(ip, {"_missing": True})
                # if it's missing in new bunch of data, mark it
                if "_missing" in new_ip_data:
                    ip_data["_missing"] = True
                    # this means it's missing second time
                    if "_missing" in old_ip_data:
                        continue
                nidg = new_ip_data.get
                oidg = old_ip_data.get
                for fn in field_names:
                    ip_data[fn] = nidg(fn, 0)
                    ip_data["%s_diff" % fn] = nidg(fn, 0) - oidg(fn, 0)
                # compute pps
                ip_data["pps"] = ip_data["pkt"] / time_diff
                tmpd[ra][ip] = ip_data
        self.data = _new_data
        if self.es:
            self.es_index_data()

    def es_index_data(self):
        record_type = "nfzoo-record"
        rotated = self.data["rotated"]
        created = self.data["created"]
        rotated_dt = datetime.datetime.utcfromtimestamp(rotated)
        created_dt = datetime.datetime.utcfromtimestamp(created)
        index = rotated_dt.strftime("nf-zoo-%Y.%m.%d-%H")
        bulk = []

        for ra, ra_data in self.data["data"].items():
            for ip, ip_data in ra_data.items():
                d = ip_data.copy()
                d.update({
                    "ra": ra,
                    "ip": ip,
                    "created": created_dt.isoformat(),
                    "rotated": rotated_dt.isoformat(),
                })
                bulk.append({
                    "_index": index,
                    "_type": record_type,
                    "_source": d,
                })
        es_helpers.bulk(self.es, bulk)

    def handle_msg(self, msg):
        LOGGER.debug("Received %s", msg.get("msg_id", "stats"))
        if msg.get("request"):
            self.handle_request_msg(msg)
        else:
            self.handle_stats_msg(msg)

    def listen(self):
        while True:
            try:
                self.handle_msg(self.zmq_pull.recv_json())
            except KeyboardInterrupt:
                LOGGER.info("The end ...")
                break
            except:
                LOGGER.exception("... booo ...")


def main():
    logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] %(message)s',
        level=logging.INFO)
    LOGGER.info("Starting %s", __file__)

    ES_KWARGS = json.loads(settings.es.kwargs)
    ZMQ_PULL_BIND = "tcp://127.0.0.1:3567"
    ZMQ_PUB_BIND = "tcp://127.0.0.1:3568"

    zmq_ctx = zmq.Context()
    zmq_pull = zmq_ctx.socket(zmq.PULL)
    zmq_pull.bind(ZMQ_PULL_BIND)
    # zmq_rep.setsockopt_string(zmq.SUBSCRIBE, u"")

    zmq_pub = zmq_ctx.socket(zmq.PUB)
    zmq_pub.bind(ZMQ_PUB_BIND)

    es = elasticsearch.Elasticsearch(**ES_KWARGS)

    MessagesHandler(zmq_pull, zmq_pub, es=es).listen()

    zmq_pull.close()
    zmq_pub.close()
    zmq_ctx.term()


if __name__ == "__main__":
    main()
