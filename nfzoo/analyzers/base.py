#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

"""
Base for cerating analyzers.
"""

import logging

LOGGER = logging.getLogger(name=__file__)


class AnalyzerBase(object):

    default_level = logging.ERROR

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self._id = None

    def get_level(self, *args, **kwargs):
        return self.kwargs.get("level", self.default_level)

    def get_msg(self, *args, **kwargs):
        return self.kwargs.get('msg') or self.default_msg

    def add_flow_record(self, ip, flow_record):
        LOGGER.debug("[%s] Received msg: %s", self._id, flow_record)

    def emit(self, *args, **kwargs):
        LOGGER.info("Event emited:\n%s\n%s", args, kwargs)
