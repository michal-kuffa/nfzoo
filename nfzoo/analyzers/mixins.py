#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

import math
import logging
import datetime
import collections
from dateutil import relativedelta

from ..utils import cached_property, import_from_string


LOGGER = logging.getLogger(name=__file__)


class ValueGetter(object):
    def __call__(self, analyzer, ip, flow_record):
        return 1


class PassThresholdMixin(object):

    default_msg = "Constant threshold exceeded"

    @cached_property
    def counter(self):
        return collections.defaultdict(float)

    def counter_reset(self, *args, **kwargs):
        del self.counter

    @cached_property
    def time_counter_interval(self):
        return relativedelta.relativedelta(
            **self.kwargs.get('time_counter_interval'))

    @cached_property
    def time_counter_last(self):
        return datetime.datetime.now()

    @cached_property
    def record_value_getter(self):
        rvg = self.kwargs.get('record_value_getter', ValueGetter())
        if isinstance(rvg, basestring):
            rvg = import_from_string(rvg)()
        return rvg

    def get_threshold(self, *args, **kwargs):
        return self.kwargs.get("threshold")

    def get_ips(self):
        return self.counter.keys()

    def get_ip_counter(self, ip):
        return self.counter[ip]

    def counter_check_ip(self, ip):
        return self.get_ip_counter(ip=ip) > self.get_threshold(ip=ip)

    def get_event_context(self, *args, **kwargs):
        ip = kwargs.pop("ip")
        return {
            "ip": ip,
            "count": self.get_ip_counter(ip=ip),
            "threshold": self.get_threshold(ip=ip),
        }

    def counter_check(self):
        # XXX (beezz) Formalize emit protocol, to not be so `raven`
        LOGGER.debug("[%s] counter state:\n%s", self._id, self.counter)
        for ip in self.get_ips():
            if self.counter_check_ip(ip=ip):
                event_context = self.get_event_context(ip=ip)
                self.emit(
                    msg=self.get_msg(**event_context),
                    tags={"ip": ip},
                    event=event_context,
                    culprit=ip,
                    level=self.get_level(**event_context),
                )
        self.counter_reset()

    def time_range_passed(self):
        to_check = self.time_counter_last + self.time_counter_interval
        now = datetime.datetime.now()
        result = now >= to_check
        if result:
            self.time_counter_last = now
        return result

    def get_record_value(self, ip, flow_record):
        return self.record_value_getter(self, ip, flow_record)

    def update_counter(self, ip, flow_record):
        self.counter[ip] += self.get_record_value(ip, flow_record)

    def add_flow_record(self, ip, flow_record):
        self.update_counter(ip, flow_record)
        if self.time_range_passed():
            self.counter_check()


class CounterMemory(object):
    def __init__(self, size=60 * 24 * 7, sample_rate=12):
        # 60 * 24 * 7 ~ one week of 5 second counter per minute
        self.size = size
        self.sample_rate = sample_rate
        self.data = collections.deque(maxlen=size)
        self._num_counter = 0
        self._cache = {}

    def add(self, counter):
        if not self._num_counter % self.sample_rate:
            self.data.append(counter)
            self._cache = {}
        self._num_counter = (self._num_counter + 1) % self.sample_rate

    def get_for_ip(self, ip, percentile=0.9):
        key = (ip, percentile)
        if key in self._cache:
            val = self._cache[key]
        else:
            val = self._cache[key] = self._get_for_ip(*key)
        return val

    def _get_for_ip(self, ip, percentile=0.9):
        values = [counter[ip] for counter in self.data if ip in counter]
        values.sort()
        if not values:
            return None
        k = (len(values) - 1) * percentile
        f = math.floor(k)
        c = math.ceil(k)
        if f == c:
            return values[int(k)]
        return values[int(f)] * (c - k) + values[int(c)] * (k - f)


class ThresholdMemoryMixin(PassThresholdMixin):

    @cached_property
    def counter_memory(self):
        return CounterMemory()

    @cached_property
    def counter_minimum(self):
        return self.kwargs.get("counter_minimum", 50)

    @cached_property
    def threshold_coeficient(self):
        return self.kwargs.get("threshold_coeficient", 1.5)

    def get_threshold(self, *args, **kwargs):
        p = self.counter_memory.get_for_ip(
            ip=kwargs.get("ip")) or self.counter_minimum
        return p * self.threshold_coeficient

    def counter_check_ip(self, ip):
        counter_value = self.get_ip_counter(ip=ip)
        return (
            counter_value > self.counter_minimum
            and
            counter_value > self.get_threshold(ip=ip)
        )

    def counter_reset(self, *args, **kwargs):
        self.counter_memory.add(self.counter)
        del self.counter
