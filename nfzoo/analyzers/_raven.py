#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

import logging

import raven

from . import base
from . import mixins


LOGGER = logging.getLogger(name=__file__)


class AnalyzerRavenMixin(object):

    @property
    def raven_client(self):
        if not hasattr(self, '_raven_client'):
            self._raven_client = raven.Client(self.kwargs.pop('sentry_dsn'))
        return self._raven_client

    def _analyzer_context(self):
        return {
            "class_path": '.'.join(
                [self.__module__, self.__class__.__name__]),
            "args": self.args,
            "kwargs": self.kwargs,
        }

    def emit(self, *args, **kwargs):
        LOGGER.info('Event emited:\n%s\n%s', args, kwargs)
        self.raven_client.captureMessage(
            "[{culprit}] {msg}".format(**kwargs),
            culprit=kwargs.get('culprit'),
            tags=kwargs.get('tags'),
            extra=kwargs.get('extra'),
            data={
                'culprit': kwargs.get('culprit'),
                'level': kwargs.get('level'),
                'nfzoo_sentry.interfaces.NfZooEvent': {
                    'analyzer': self._analyzer_context(),
                    'event': kwargs.get('event'),
                }
            },
        )


class ConstantThresholdRaven(
    mixins.PassThresholdMixin,
    AnalyzerRavenMixin,
    base.AnalyzerBase,
):
    pass


class MemoryThresholdRaven(
    mixins.ThresholdMemoryMixin,
    AnalyzerRavenMixin,
    base.AnalyzerBase,
):
    pass
