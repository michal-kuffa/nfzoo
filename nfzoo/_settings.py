#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@T500>


import os
import os.path
import json

BASE_DIR = os.path.dirname(__file__)

# NF_DUMP_MAPPING = json.load(
#     open(os.path.join(BASE_DIR, "mappings", "nf-dump.json")))

ES_KWARGS = {
    "hosts": [
        {"host": "192.168.8.2"},
        {"host": "192.168.8.3"},
        {"host": "192.168.8.4"},
    ],
    "timeout": 30,
}

NUM_OF_THREADS = 20


ZMQ_BIND = "tcp://192.168.8.2:5556"
