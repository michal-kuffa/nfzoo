#!/usr/bin/env python

import zmq
# import json
import time
import Queue
# import pprint
import logging
import threading
import datetime
# import dateutil.parser
from elasticsearch import Elasticsearch
from elasticsearch import helpers as es_helpers

from .utils import pre_process_data


LOGGER = logging.getLogger(name=__file__)


def get_index(ts=None, min_rotate=10):
    ts = time.time() if ts is None else ts
    dt = datetime.datetime.utcfromtimestamp(ts)
    return dt.strftime("nf-dump-%Y.%m.%d-%H")


def saver_thread(q, es_kwargs):
    es = Elasticsearch(**es_kwargs)
    bulk = []
    while True:
        try:
            timeout_passed = False
            try:
                _d = q.get(timeout=10)
                d = pre_process_data(
                    _d,
                    (
                        'ts', 'tr', 'tfs', 'te', 'in', 'bpp',
                        'bps', 'pkt', 'byt', 'sa', 'da', 'pr',
                        'sp', 'dp', 'sap', 'dap', 'flg', 'ra',
                        'it', 'ic', 'nh', 'nhb'
                    ),

                )
                bulk.append({
                    "_index": get_index(),
                    "_type": "flow-record",
                    "_source": d,
                })
                q.task_done()
            except Queue.Empty:
                timeout_passed = True
            if len(bulk) > 200 or (len(bulk) and timeout_passed):
                LOGGER.info(
                    "Saving [%d] netflow data. Queue-size [%s]",
                    len(bulk),
                    q.qsize(),
                )
                es_helpers.bulk(es, bulk)
                bulk = []
        except Exception:
            LOGGER.exception("Saver exception ...")


def main():

    import _settings as settings

    logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] %(message)s',
        level=logging.INFO)

    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(settings.ZMQ_BIND)
    socket.setsockopt_string(zmq.SUBSCRIBE, u"")

    q = Queue.Queue(maxsize=2 ** 18)
    for _ in range(settings.NUM_OF_THREADS):
        es_saver = threading.Thread(
            target=saver_thread,
            args=(q, settings.ES_KWARGS))
        es_saver.daemon = True
        es_saver.start()

    while True:
        try:
            flow_record = socket.recv_json()
        except ValueError:
            LOGGER.exception("Not json in ZMQ ...`%s`", flow_record)
            continue
        q.put(flow_record)


if __name__ == "__main__":
    main()
