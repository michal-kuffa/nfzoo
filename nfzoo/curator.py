#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@T500>

"""

nfzoo.curator
~~~~~~~~~~~~~

Subscribe to netflow messages and compute stats.
"""

import zmq
import logging
import collections

import time

from .utils import pre_process_data
from .config import settings

LOGGER = logging.getLogger(name=__file__)


def _defdict_int_factory():
    return collections.defaultdict(int)


def _nested_defdict_int_factory():
    return collections.defaultdict(_defdict_int_factory)


class StatsHolder(object):

    def __init__(self, zmq_push, rotation_interval=60):
        self.zmq_push = zmq_push
        self.rotation_interval = rotation_interval
        self.created = None
        self.rotate(publish=False)

    def rotate(self, publish=True):
        LOGGER.info("Rotating data created: %s", self.created)
        if publish:
            LOGGER.info("Publishing data before rotating")
            self.zmq_push.send_json({
                "stats": True,
                "created": self.created,
                "rotated": time.time(),
                "data": self.data,
            })
        self.data = collections.defaultdict(_nested_defdict_int_factory)
        self.created = time.time()
        self.rotate_time = self.created + self.rotation_interval

    def update_from_msg(self, msg, rotate=True):
        router_data = self.data[msg["ra"]]
        sad = router_data[msg["sa"]]
        dad = router_data[msg["da"]]

        sad["conn_count"] += 1
        sad["out_conn_count"] += 1

        dad["conn_count"] += 1
        dad["in_conn_count"] += 1

        sad["out_byt"] += msg["byt"]
        dad["in_byt"] += msg["byt"]

        sad["pkt"] += msg["pkt"]
        dad["pkt"] += msg["pkt"]

        if rotate and self.rotate_time < time.time():
            self.rotate()


def main():
    logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] %(message)s',
        level=logging.INFO)
    LOGGER.info("Starting %s", __file__)

    ZMQ_SUB_BIND = settings.nfcapd.zmq_publisher
    ZMQ_PUSH_BIND = settings.nfzoo.curator.zmq_pull

    zmq_ctx = zmq.Context()
    zmq_sub = zmq_ctx.socket(zmq.SUB)
    zmq_sub.connect(ZMQ_SUB_BIND)
    zmq_sub.setsockopt_string(zmq.SUBSCRIBE, u"")

    zmq_push = zmq_ctx.socket(zmq.PUSH)
    zmq_push.connect(ZMQ_PUSH_BIND)

    stats = StatsHolder(zmq_push=zmq_push, rotation_interval=80)

    while True:
        try:
            flow_record = pre_process_data(zmq_sub.recv_json())
            stats.update_from_msg(flow_record)
            LOGGER.debug(
                "Received: %15s -> %15s %5s",
                flow_record["sa"],
                flow_record["da"],
                flow_record["pr"],
            )
        except KeyboardInterrupt:
            LOGGER.info("The end ...")
            break
        except:
            LOGGER.exception("... booo ...")

    zmq_sub.close()
    zmq_push.close()
    zmq_ctx.term()


if __name__ == "__main__":
    main()
