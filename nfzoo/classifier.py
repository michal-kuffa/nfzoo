#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

"""

nfzoo.classifier
~~~~~~~~~~~~~~~~

Classify netflow messages by prepending destination ip address to message.

"""

import zmq
import json
import logging

from .config import settings

LOGGER = logging.getLogger(name=__file__)


def main():
    logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] %(message)s',
        level=logging.INFO)
    LOGGER.info("Starting %s", __file__)

    ZMQ_SUB_BIND = settings.nfcapd.zmq_publisher
    LOGGER.info("Binding subscriber zmq socket: %s", ZMQ_SUB_BIND)

    ZMQ_PUB_BIND = settings.nfzoo.classifier.zmq_publisher
    LOGGER.info("Binding publisher zmq socket: %s", ZMQ_PUB_BIND)

    zmq_ctx = zmq.Context()
    zmq_sub = zmq_ctx.socket(zmq.SUB)
    zmq_sub.connect(ZMQ_SUB_BIND)
    zmq_sub.setsockopt_string(zmq.SUBSCRIBE, u"")

    zmq_pub = zmq_ctx.socket(zmq.PUB)
    zmq_pub.bind(ZMQ_PUB_BIND)

    while True:
        try:
            flow_record = zmq_sub.recv_json()
            zmq_pub.send_multipart(
                (
                    flow_record["da"].strip().encode('utf-8'),
                    json.dumps(flow_record)
                )
            )
            zmq_pub.send_multipart(
                (
                    flow_record["sa"].strip().encode('utf-8'),
                    json.dumps(flow_record)
                )
            )
            LOGGER.debug("Received: %15s", flow_record["da"])
        except KeyboardInterrupt:
            LOGGER.info("The end ...")
            break
        except:
            LOGGER.exception("... booo ...")

    zmq_sub.close()
    zmq_pub.close()
    zmq_ctx.term()


if __name__ == "__main__":
    main()
