#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@T500>

"""

nfzoo.visitor
~~~~~~~~~~~~~

Query keeper's stats by ip (range).

"""

import sys
import zmq
import uuid
import time
import logging
import argparse
import datetime
from .config import settings, configer, ConfigException


LOGGER = logging.getLogger(name=__file__)


def _dummy_fn(val):
    return val


def scale_to_mb(val):
    return float(val) / (2 ** 20)

DEFAULT_DATA_CONF = {
    "ip": (_dummy_fn, 0),
    "in_conn_count": (_dummy_fn, 0),
    "in_conn_count_diff": (_dummy_fn, 0),
    "out_conn_count": (_dummy_fn, 0),
    "out_conn_count_diff": (_dummy_fn, 0),
    "in_byt": (scale_to_mb, 0),
    "in_byt_diff": (scale_to_mb, 0),
    "out_byt": (scale_to_mb, 0),
    "out_byt_diff": (scale_to_mb, 0),
}

DEFAULT_HEADER_CONF = {
    "ip": (_dummy_fn, "IP ADDRESS"),
    "in_conn_count": (_dummy_fn, "IN CONNS"),
    "in_conn_count_diff": (_dummy_fn, "DIFF"),
    "out_conn_count": (_dummy_fn, "OUT CONNS"),
    "out_conn_count_diff": (_dummy_fn, "DIFF"),
    "in_byt": (_dummy_fn, "IN MB"),
    "in_byt_diff": (_dummy_fn, "DIFF"),
    "out_byt": (_dummy_fn, "OUT MB"),
    "out_byt_diff": (_dummy_fn, "DIFF"),
}

DEFAULT_HEADER_FMT = (
    " {ip:<20}"
    " {in_conn_count:>9}"
    " / {in_conn_count_diff:<5}"
    " {out_conn_count:>9}"
    " / {out_conn_count_diff:<5}"
    " {in_byt:>10}"
    " / {in_byt_diff:<10}"
    " {out_byt:>10}"
    " / {out_byt_diff:<10}"
)

DEFAULT_ROW_FMT = (
    " {ip:<20}"
    " {in_conn_count:>9}"
    " / {in_conn_count_diff:<5}"
    " {out_conn_count:>9}"
    " / {out_conn_count_diff:<5}"
    " {in_byt:>10.4f}"
    " / {in_byt_diff:<10.4f}"
    " {out_byt:>10.4f}"
    " / {out_byt_diff:<10.4f}"
)


def format_ip_data(ip_data, data_conf=None, fmt=None):
    d = {}
    data_conf = DEFAULT_DATA_CONF if data_conf is None else data_conf
    fmt = DEFAULT_ROW_FMT if fmt is None else fmt
    for fn, (val_fn, def_val) in data_conf.items():
        d[fn] = val_fn(ip_data.get(fn, def_val))
    return fmt.format(**d)


def _main():

    cli_parser = argparse.ArgumentParser(description=__doc__)
    cli_parser.add_argument(
        "--sum", dest="sum", action="store_true", default=False,
        help="Sum stats for all ip addresses from selected network.",
    )
    cli_parser.add_argument(
        "--total", dest="total", action="store_true", default=False,
        help="Get summary stats across all routers.",
    )
    cli_parser.add_argument(
        "--conf", dest="config", action="store",
        help=(
            "Configuration file path. If omitted, environment"
            " variable NFZOO_CONFIG is used."
        ),
    )
    cli_parser.add_argument(
        "ip",
        nargs="?",
        metavar="IPNET",
        action="store",
        default="0.0.0.0/0",
        help="IP address or network",
    )
    cli_args = cli_parser.parse_args()

    if cli_args.config:
        configer.configure_from_files([cli_args.config, ])

    logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] %(message)s',
        level=logging.CRITICAL)
    LOGGER.info("Starting %s", __file__)

    # check configuration at start
    ZMQ_SUB_BIND = settings.nfzoo.curator.zmq_publisher
    ZMQ_PUSH_BIND = settings.nfzoo.curator.zmq_pull

    zmq_ctx = zmq.Context()
    zmq_sub = zmq_ctx.socket(zmq.SUB)
    zmq_sub.connect(ZMQ_SUB_BIND)
    zmq_sub.setsockopt_string(zmq.SUBSCRIBE, u"")

    zmq_push = zmq_ctx.socket(zmq.PUSH)
    zmq_push.connect(ZMQ_PUSH_BIND)

    msg_id = str(uuid.uuid1())
    msg = {
        "request": True,
        "sum": cli_args.sum,
        "total": cli_args.total,
        "msg_id": msg_id,
        "ip": cli_args.ip,
    }

    zmq_push.send_json(msg)

    while True:
        try:
            recv_msg = zmq_sub.recv_json()
            if recv_msg["msg_id"] == msg_id:
                if "error" in recv_msg:
                    print "Error: {error}".format(**recv_msg)
                else:
                    created = datetime.datetime.fromtimestamp(
                        recv_msg["created"] or time.time()
                    )
                    rotated = datetime.datetime.fromtimestamp(
                        recv_msg["rotated"] or time.time()
                    )
                    for ra, router_data in recv_msg["data"].items():
                        # print header
                        header_row = format_ip_data(
                            {},
                            data_conf=DEFAULT_HEADER_CONF,
                            fmt=DEFAULT_HEADER_FMT,
                        )
                        print "-" * len(header_row)
                        print " ROUTER {ra} | {created} -> {rotated}".format(
                            ra=ra,
                            created=created.isoformat(),
                            rotated=rotated.isoformat(),
                        )
                        print header_row
                        print "-" * len(header_row)
                        # print data
                        for ip, ip_data in router_data:
                            ip_data["ip"] = ip
                            print format_ip_data(ip_data)
                break
        except KeyboardInterrupt:
            break

    zmq_sub.close()
    zmq_push.close()
    zmq_ctx.term()


def main():
    try:
        _main()
    except ConfigException as exc:
        sys.exit(exc.message)


if __name__ == "__main__":
    main()
