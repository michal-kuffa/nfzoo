#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@T500>

# std
import logging
import datetime
import importlib

# 3rd party
from geoip import geolite2

# constants
_max_ts = (1 << 31) - 1
LOGGER = logging.getLogger(name=__file__)


class cached_property(object):
    """
    Stolen from pydanny's:
    https://github.com/pydanny/cached-property

    A property that is only computed once per instance and then replaces itself
    with an ordinary attribute. Deleting the attribute resets the property.
    Source: https://github.com/bottlepy/bottle/commit/fa7733e075da0d790d809aa3d2f53071897e6f76
    """  # noqa

    def __init__(self, func):
        self.__doc__ = getattr(func, '__doc__')
        self.func = func

    def __get__(self, obj, cls):
        if obj is None:
            return self
        value = obj.__dict__[self.func.__name__] = self.func(obj)
        return value


def import_from_string(class_path):
    module_name, class_name = class_path.rsplit('.', 1)
    return getattr(importlib.import_module(module_name), class_name)


def process_timestamp(v):
    fv = float(v)
    # avoid timestamp out of range for 32bit
    if fv > _max_ts:
        fv /= 100
    return datetime.datetime.utcfromtimestamp(fv)


def pre_process_data(d, fields=None):
    _d = {}
    iter_keys = fields or d.keys()
    for key in iter_keys:
        value = d[key].strip()
        try:
            _d[key] = {
                "al": float,
                "bps": int,
                "in": int,
                "byt": int,
                "ibyt": int,
                "obyt": int,
                "pkt": int,
                "ipkt": int,
                "opkt": int,
                "bpp": int,
                "ts": process_timestamp,
                "tr": process_timestamp,
                "tfs": process_timestamp,
                "te": process_timestamp,
            }.get(key, str)(value)
        except ValueError:
            LOGGER.exception("Can't convert field %s, val: %s", key, value)
            _d[key] = value
    # try to resolve geo info for sa and da ips
    for field_name in ("da", "sa"):
        geo_match = geolite2.lookup(_d[field_name])
        if geo_match:
            if geo_match.location:
                _d["%s_location" % field_name] = "{0},{1}".format(
                    *geo_match.location)
            if geo_match.country:
                _d["%s_country" % field_name] = geo_match.country
    return _d
