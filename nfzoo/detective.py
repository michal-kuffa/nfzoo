#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 beezz <beezz@beezz-t500>
#
# Distributed under terms of the MIT license.

"""

nfzoo.detective
~~~~~~~~~~~~~~~~

Subscribe to messages a try to detect anomalities.

"""

import zmq
import IPy
import json
import logging
import argparse

from .config import settings
from .utils import pre_process_data, import_from_string

LOGGER = logging.getLogger(name=__file__)


class AnalyzersManager(object):

    def __init__(self, analyzers_config, **kwargs):
        self.analyzers_config = analyzers_config
        self.kwargs = kwargs
        self._analyzers = {}
        self._create_analyzers()

    def _create_analyzers(self):
        for analyzers_config in self.analyzers_config:
            self._analyzers[analyzers_config['id']] = import_from_string(
                analyzers_config['class_path']
            )(
                *analyzers_config.get('args', ()),
                **analyzers_config.get('kwargs', {})
            )
            self._analyzers[analyzers_config['id']]._id = analyzers_config['id']

    def add_flow_record(self, ip, flow_record):
        for analyzer in self._analyzers.values():
            analyzer.add_flow_record(ip, flow_record)



def _cli_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "ranges", metavar="IPRANGE", type=IPy.IP, nargs="+",
        help="IP ranges for which to monitor netflow anomalies.",
    )
    return parser


def main():
    logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] %(message)s',
        level=logging.DEBUG)
    LOGGER.info("Starting %s", __file__)

    # 46.229.2270/25
    # 109.74.151.128/25
    # 93.184.66.128/26
    # 93.184.66.192/26

    ANALYZERS = [
        {
            'id': '1',
            'class_path': 'nfzoo.analyzers._raven.ConstantThresholdRaven',
            'kwargs': {
                "threshold": 1500,
                "time_counter_interval": {"seconds": 5},
                "sentry_dsn": 'http://92b9330b13fa4bc3a657039284f3b4fb:6942091a6eb24481a802e1286c8de258@192.168.8.4:9000/2',
            },
        },
        {
            'id': '2',
            'class_path': 'nfzoo.analyzers._raven.MemoryThresholdRaven',
            'kwargs': {
                "msg": "Counter exceeded 150% of 90% percentile.",
                "time_counter_interval": {"seconds": 5},
                "sentry_dsn": 'http://92b9330b13fa4bc3a657039284f3b4fb:6942091a6eb24481a802e1286c8de258@192.168.8.4:9000/2',
            },
        },
        {
            'id': '3',
            'class_path': 'nfzoo.analyzers._raven.MemoryThresholdRaven',
            'kwargs': {
                "msg": "Sum of bytes exceeded 150% of 90% percentile.",
                "counter_minimum": 5000,
                "record_value_getter": lambda a, ip, fr: fr["byt"],
                "time_counter_interval": {"seconds": 5},
                "sentry_dsn": 'http://92b9330b13fa4bc3a657039284f3b4fb:6942091a6eb24481a802e1286c8de258@192.168.8.4:9000/2',
            },
        }
    ]

    analyzers_manager = AnalyzersManager(ANALYZERS)

    ZMQ_SUB_BIND = settings.nfzoo.classifier.zmq_publisher
    LOGGER.info("Binding subscriber zmq socket: %s", ZMQ_SUB_BIND)

    zmq_ctx = zmq.Context()
    zmq_sub = zmq_ctx.socket(zmq.SUB)
    zmq_sub.connect(ZMQ_SUB_BIND)

    parser = _cli_parser()
    args = parser.parse_args()

    # set socket options
    # to receive messages only for
    # given IP ranges
    for ip_range in args.ranges:
        LOGGER.debug("Settings range:%s", ip_range)
        for ip in ip_range:
            LOGGER.debug(">>>> ip: %s", ip)
            zmq_sub.setsockopt(zmq.SUBSCRIBE, str(ip))

    while True:
        try:
            ip, flow_record_raw = zmq_sub.recv_multipart()
            flow_record = pre_process_data(json.loads(flow_record_raw))
            analyzers_manager.add_flow_record(ip, flow_record)
            LOGGER.debug("Received: %s", flow_record)
        except KeyboardInterrupt:
            LOGGER.info("The end ...")
            break
        except:
            LOGGER.exception("... booo ...")

    zmq_sub.close()
    zmq_ctx.term()


if __name__ == "__main__":
    main()
