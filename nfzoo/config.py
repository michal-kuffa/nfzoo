#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

"""

nfzoo.config
~~~~~~~~~~~~

Configuration module.

"""

import os
import logging
import ConfigParser


LOGGER = logging.getLogger(__file__)


class ConfigException(Exception):
    pass


class AttrsBag(object):
    """Just attributtes holder."""

    def __init__(self, **kw):
        for k, v in kw.items():
            setattr(self, k, v)

    def __str__(self):
        return self.__dict__.__str__()

    def __repr__(self):
        return self.__dict__.__repr__()

    def __getattr__(self, attr_name):
        raise ConfigException(
            "Setting `{0}.{1}` is not configured.".format(
                self.__dict__.get("_name", ""), attr_name,
            )
        )


class RootSettingsBag(AttrsBag):

    def __getattr__(self, attr_name):
        raise ConfigException(
            "Section `{0}` not configured."
            " Do you have NFZOO_CONFIG exported and pointing"
            " to nfzoo configuration file ?".format(attr_name)
        )


class SettingsConfiger(object):
    """Given any object as `holder`, sets it's atributes
    form ini files or dicts.

    For now there are three restrictions:
        * only two levels of section/sub sections are allowed
        * sub section name can't be the same as section option
          (otherwise it'll be overriden)

    For naming use python object attributes friendly names:
        * not starting with number
        * only special character is underscore
        etc.
    """

    def __init__(self, holder):
        self.holder = holder

    def add_section(self, name, values, obj=None):
        obj = self.holder if obj is None else obj
        # _name = name
        sub_section = name
        if '/' in name:
            # for now only two levels are allowed
            parent_section, sub_section = name.split('/')
            if not hasattr(obj, parent_section):
                setattr(obj, parent_section, AttrsBag(_name=parent_section))
            obj = getattr(obj, parent_section)
        setattr(obj, sub_section, AttrsBag(_name=name, **values))

    def configure_from_files(self, paths, obj=None):
        obj = self.holder if obj is None else obj
        config = ConfigParser.ConfigParser()
        successful_paths = config.read(paths)
        LOGGER.info("Successfull paths red: %s", successful_paths)
        self._add_ini(config, obj=obj)

    def _add_ini(self, config, obj=None):
        obj = self.holder if obj is None else obj
        for section in config.sections():
            self.add_section(
                section,
                dict([
                    (v_name, config.get(section, v_name))
                    for v_name in config.options(section)
                ]),
                obj=obj,
            )

# let's have settings importable
settings = RootSettingsBag()
configer = SettingsConfiger(settings)

if 'NFZOO_CONFIG' in os.environ:
    configer.configure_from_files([os.environ['NFZOO_CONFIG'], ])
