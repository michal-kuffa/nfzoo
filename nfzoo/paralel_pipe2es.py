#!/usr/bin/env python

import zmq
import time
import logging
import datetime
from elasticsearch import Elasticsearch
from elasticsearch import helpers as es_helpers

from .utils import pre_process_data


LOGGER = logging.getLogger(name=__file__)

DATA_FIELDS = (
    'ts', 'tr', 'tfs', 'te', 'in', 'out', 'bpp',
    'bps', 'pkt', 'byt', 'sa', 'da', 'pr',
    'sp', 'dp', 'sap', 'dap', 'flg', 'ra',
    'it', 'ic', 'nh', 'nhb'
)


def get_index(ts=None):
    ts = time.time() if ts is None else ts
    dt = datetime.datetime.utcfromtimestamp(ts)
    return dt.strftime("nf-dump-%Y.%m.%d-%H")


def prepare_for_indexing(flow_record, fields=DATA_FIELDS):
    return {
        "_index": get_index(),
        "_type": "flow-record",
        "_source": pre_process_data(flow_record, fields),
    }


def flow_record_producer(zmq_socket):
    while True:
        try:
            flow_record = zmq_socket.recv_json()
        except ValueError:
            LOGGER.exception("Not json in ZMQ ...`%s`", flow_record)
            continue
        yield prepare_for_indexing(flow_record)


def main():
    import _settings as settings
    logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] %(message)s',
        level=logging.INFO)
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(settings.ZMQ_BIND)
    socket.setsockopt_string(zmq.SUBSCRIBE, u"")
    for fr in flow_record_producer(socket):
        print(fr)
    # es = Elasticsearch(**settings.ES_KWARGS)
    # es_helpers.parallel_bulk(es, flow_record_producer(socket))


if __name__ == "__main__":
    main()
